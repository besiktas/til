from asyncore import write
import os
import logging


def clean_name(name):
    """ """
    replace_these = ["\n", "##", "#", ":"]
    replace_with_space = ["-"]
    for x in replace_these:
        name = name.replace(x, "")
    for x in replace_with_space:
        name = name.replace(x, " ")
    if name[0] == " ":
        print(name)
        # name = name[1:]
    return name.lower()


def write_files_for_dir(dir_name):
    """ """
    section = "## " + dir_name + "\n"
    section = section.replace("-", " ")
    files = os.listdir(dir_name)
    files.sort(key=lambda x: x[0].lower())
    for f in files:
        if f in [".DS_Store"] or f[-3:] in [".go", ".py"]:
            continue
        logging.info("going thrugh: ", f)
        # section += '\n'
        title = ""
        link = ""
        file_location = os.path.join(dir_name, f)
        try:
            with open(file_location, "r", encoding="utf-8") as t:
                title = t.readline()
        except IsADirectoryError:
            logging.info("not a file:_", file_location)
            continue
        title = clean_name(title)
        logging.info("new title", title)
        link = "[" + title + "](" + file_location + ")"
        section += "\n" + link + "\n"
    # line break at end of section before next section
    section += "\n"
    return section


def append_section(file, to_append, write_out: bool = True):
    with open(to_append, "r", encoding="utf-8") as f:
        append = f.readlines()
    append = "".join(append)

    if write_out:
        with open(file, "a", encoding="utf-8") as f:
            f.write(append)


if __name__ == "__main__":
    import sys

    ignore = [".DS_Store", ".git", "A-CUSTOM-THEME", "public"]
    other = ["courses", "resources", "read", "to read"]

    write_out = True if "--write" in sys.argv else False
    total_file = "# General List of TIL\n\n"
    folders = os.listdir()
    folders.sort(key=lambda x: x[0].lower())
    for d in folders:
        if d not in other + ignore:
            print("using d", d)
            try:
                cur_dir_files = os.listdir(d)
                to_write = write_files_for_dir(d)
                total_file += to_write
            except NotADirectoryError:
                logging.info("not using...", d)
    # print(total_file)

    if write_out:
        with open("index.md", "w", encoding="utf-8") as f:
            f.write(total_file)
    append_section("index.md", "base.md")
