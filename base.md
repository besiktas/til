# Books and White Papers Read <sup><a href="#fn1">[1]</a></sup>

[investing thing](/read/papers/Improving Factor-Based Quantitative Investing by Forecasting Company Fundamentals.md)

[deep learning related](read/deep-learning-papers.md)

[larger form books](read/books.md)


# Online Courses and Classes

[courses list](courses/readme.md)

# NLP and Deep Learning Resources

[lists](resources/lists.md)


----------

#### Side note:
Some Pages have lines listed below at the top which is something that syncs between evernote and markdown
```
---
title: Papers
tags: []
notebook: TIL
---
```

----------

<p><a name="fn1">[1]<a href="https://github.com/dennybritz/deeplearning-papernotes">inspired by deeplearning papernotes</a></a></p>