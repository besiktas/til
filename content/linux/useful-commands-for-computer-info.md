# some useful commands for hardware info


## OS info
- `cat /etc/lsb-release`

## memory
- `cat /proc/meminfo`

## 32 or 64 bit

- `getconf LONG_BIT`

## cpu info
- `lscpu`
- `cat /proc/cpuinfo`
