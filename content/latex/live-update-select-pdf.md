---
title: "convert flac to mp3"
date: 2018-09-18
---

continuous update and view file
```
latexmk -pdf -pvc $FILE
```


to not use a pdf viewer (for instance skim seems better)
```
latexmk -pdf -pvc -view=none $FILE
```