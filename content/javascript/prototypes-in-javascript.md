# prototypes in javascript

from [this site](http://javascriptissexy.com/javascript-prototype-in-plain-detailed-language/):

> This in essence is the prototype chain: the chain from an object’s prototype to its prototype’s prototype and onwards. And JavaScript uses this prototype chain to look for properties and methods of an object. 