# Useful commands:


LVM commands

- `vgdisplay`
    - display volumegroup
- `lvdisplay`
    - display logical volume
- `resize2fs /dev/vg0/home`