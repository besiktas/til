# to open and view web dashboard

- list current context:

```
> kubectl config current-context
gke_kip-ai_us-east1-b_nlp-tester
```

- get login/password:

```
> kubectl config view
users:
- name: gke_kip-ai_us-east1-b_nlp-tester
  user:
    client-certificate-data: REDACTED
    client-key-data: REDACTED
    password: REDACTED
    username: admin
```

or if that is using service tokens/whatever you can use:

```
gcloud container clusters describe "cluster-name"
```

- get ip of kubernetes cluster dashboard:

```
> kubectl cluster-info
Kubernetes master is running at https://ip____
GLBCDefaultBackend is running at https://ip____/api/v1/proxy/namespaces/kube-system/services/default-http-backend
Heapster is running at https://ip____/api/v1/proxy/namespaces/kube-system/services/heapster
KubeDNS is running at https://ip____/api/v1/proxy/namespaces/kube-system/services/kube-dns
kubernetes-dashboard is running at https://ip____/api/v1/proxy/namespaces/kube-system/services/kubernetes-dashboard
```

- use kubernetes-dashword link with username and password