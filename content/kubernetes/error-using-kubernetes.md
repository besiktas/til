# Various errors and how to fix them on kubernetes:

## error about connecting and deleting ~/.kube/config does not work:

if this error pops up:
```
 error: failed to negotiate an api version; server supports: map[], client supports: map[batch/v2alpha1:{} policy/v1alpha1:{} rbac.authorization.k8s.io/v1alpha1:{} v1:{} apps/v1alpha1:{} authorization.k8s.io/v1beta1:{} autoscaling/v1:{} batch/v1:{} componentconfig/v1alpha1:{} authentication.k8s.io/v1beta1:{} extensions/v1beta1:{} federation/v1beta1:{}]
```


you can use

```
> gcloud config set container/use_client_certificate True
> gcloud container clusters get-credentials kip
```

to fix it