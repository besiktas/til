# switching clusters (i.e. between person A's and person B's cluster)


list configs
```
> gcloud config configurations list
NAME     IS_ACTIVE  ACCOUNT                      PROJECT     DEFAULT_ZONE  DEFAULT_REGION
default  False       graham@.com  kip-s  us-east1-b    us-east1
kip-2   True      graham@.com  kip-2      us-east1-b    us-east1
```

switch from say kip-2 to default (assuming we are already in kip-2 as shown by kip-2 IS_ACTIVE):
```
> gcloud config configurations activate default
```


list clusters
```
> gcloud container clusters list
NAME        ZONE        MASTER_VERSION  MASTER_IP        MACHINE_TYPE   NODE_VERSION  NUM_NODES  STATUS
nlp-tester  us-east1-b  1.3.2           104.X.X.X  n1-standard-1  1.         3          RUNNING
```

activate/switch into cluster
```
> gcloud container clusters get-credentials nlp-tester
```