# Allow external traffic

for some pod started with:

```
kubectl run rnn --image=gcr.io/kip-styles/rnn:0.7.5 --port=8085
```

to expose it externally use `kubectl expose deployment rnn --type=LoadBalancer`

without quotes around LoadBalencer