# Delete a pod with status unknown or wont terminate

if you see:
```
slack-49400829-7rttv                  0/1       Unknown             0          54m
```

kubectl delete pods slack-49400829-7rttv --grace-period=0 --force
