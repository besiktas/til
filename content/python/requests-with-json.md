# Using Requests

For something that in node would be

```
var resp =request({
...       method: 'POST',
...       url: config.nlp_rnn + '/predict',
...       json: true,
...       body: {
.....         text: 'red shoes',
.....       }
...     })
```

the equivalent in python is

```
import requests
r = requests.post('http://localhost:8085/predict', json={'text': 'red shoes'})
```

the json argument will do what you would otherwise have to do as

```
r = requests.post('http://localhost:8085/predict', headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}, data=json.dumps({'text': 'red shoes'})
```

but it wont work without headers and json.dumps.