# to use pipenv and pm2 to reload for instance

```
pm2 start app/server.py --watch --interpreter=`pipenv --py`
```
