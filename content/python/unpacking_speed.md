---
title: "unpacking object speed"
date: 2022-09-15
---

was curious about unpacking an object as a data type when you may be doing this many times for a training algorithm.  Seems like list comprehensions are the quickest way to go about this for instance the following data structure might be similar to some data in a pytorch training loop:

```python
import torch


class Sample:
    def __init__(self):
        self.data = torch.rand(3, 10, 10)
        self.target = torch.randint(10, (1,))


class Samples:
    def __init__(self, n: int = 10) -> None:
        self.samples = [Sample() for s in range(n)]

    def testA(self):

        self.data, self.target = list(zip(*((s.data, s.target) for s in self.samples)))

    def testB(self):
        self.data = [s.data for s in self.samples]
        self.target = [s.target for s in self.samples]


if __name__ == "__main__":
    import timeit

    s = Samples()
    number = 500000

    tA = timeit.timeit("Samples().testA()", globals=globals(), number=number)
    tB = timeit.timeit("Samples().testB()", globals=globals(), number=number)
    print("tA:", tA)
    print("tB:", tB)
```