# Drop into ipython console after running script:

Use `ipython -i --` then script name and arguments.  Very useful for running experiment and then looking at results

i.e.

```
ipython3 -i -- glove-python/examples/example.py  -c text.file.txt
```