# Not actually a simple TIL but some tips

- If unicode being translated into escape or something and just want to disregard it

```
> text = 'women’s jacket'
> text = text.encode('ascii','ignore')
> print(text)
b'womens jacket'
```