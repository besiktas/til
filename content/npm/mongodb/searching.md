# Searching for documents

+ Searching for items with greater than length:
    > db.getCollection('messages').find( {$where: "this.msg.length > 5"})

+ Searching for text with wildcards around it:
    > db.getCollection('messages').find({msg: /below/})


+ All together:

```db.getCollection('messages').find( {$where: "this.msg.length > 5", msg: /below /, incoming: true})```


Lots of extra good examples here: https://docs.mongodb.com/manual/reference/sql-comparison/