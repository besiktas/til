# Chaining commands in mongodb

when chaining commands for mongoose objects, must end with .exec() to finally execute the update, find, etc.

this is because if not using callbacks, would be ambiguous otherwise when to execute command.