## using git with diff-so-fancy


use `git dsf`
which has ~/.gitconfig alias:

```
[alias]
    dsf = "!f() { [ \"$GIT_PREFIX\" != \"\" ] && cd "$GIT_PREFIX"; git diff --color $@ | diff-so-fancy | less --tabs=4 -RFX; }; f"
```

from https://github.com/so-fancy/diff-so-fancy
