

class Chest:
    def __init__(self, cost, prizes):
        self.cost = cost
        self.prizes = prizes

    def __repr__(self):
        return str(self.cost)


class Prize:
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return str(self.name)

    def __repr__(self):
        return self.name



## prizes
teddy = Prize('teddy')
lollipop = Prize('lollipop')
car = Prize('car')
ring = Prize('ring')

##

c1 = Chest(5, [teddy, lollipop, car])
c2 = Chest(2, [teddy])
c3 = Chest(4, [car, ring])

chests = [c1, c2, c3]


prize = [teddy, ring]

def findChests(chests, prizes):
    goodChests = set()
    for c in chests:
        for p in prizes:
            if p in c.prizes:
                goodChests.add(c)


    return sum([c.cost for c in goodChests])



print(findChests(chests, prize))