# from recent interview 

recent interview made a big deal about not using p = p[::-1] or anything that outright replaced the non ascii chars in a string.  i understand the idea was to make me use a loop and not pythonic code but attempted to use a for loop with p[n] ==  p[len(p) - n] type deal (paraphrasing) but realized this is probably the most simple way to do this that i thought up

```
from string import ascii_lowercase


def main(p):
    l_char = 0
    r_char = len(p) - 1

    while l_char < r_char:
        if p[l_char].lower() not in ascii_lowercase:
            l_char += 1
            continue
        if p[r_char].lower() not in ascii_lowercase:
            r_char -= 1
            continue
        if p[l_char].lower() == p[r_char].lower():
            l_char += 1
            r_char -= 1
        else:
            return False
    return True


test1 = 'asdffdsa'
test2 = 'asdfdsa'
test21 = 'asdfdsaa'
test3 = 'a man, a plan, a canal... panama!'
test4 = 'a man, a plan, a canal... zpanama!'


print(main(test1))
print(main(test2))
print(main(test21))
print(main(test3))
print(main(test4))
```