# simple commands

## copy files from local to remote

```> gcloud compute copy-files [LOCAL_FILE_PATH] [INSTANCE_NAME]:~/```

i.e:

```> gcloud compute copy-files localfile.txt gcloud-instance:~/```

also possible to go from remote to local

```> gcloud compute copy-files [INSTANCE_NAME]:[REMOTE_FILE_PATH] [LOCAL_FILE_PATH]```

## list compute instances

```> gcloud compute instances list```