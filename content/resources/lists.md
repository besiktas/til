# Other Resources:

+ [Deep Learning for NLP resources](https://github.com/andrewt3000/DL4NLP)

+ [Awesome Recurrent Neural Networks](https://github.com/kjw0612/awesome-rnn)

+ [Deep Learning for NLP](https://github.com/shashankg7/Deep-Learning-for-NLP-Resources)

+ [Deep Learning for NLP](http://www.socher.org/index.php/DeepLearningTutorial/DeepLearningTutorial)

+ [Awesome Remote Job Stuff](https://github.com/lukasz-madon/awesome-remote-job)

# Other Assorted Resources

+ http://www.wildml.com/2016/04/deep-learning-for-chatbots-part-1-introduction/
    + Various links at bottom