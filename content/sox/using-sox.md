---
title: "using sox"
date: 2018-09-13
---

# to combine multiple audio files into one
`sox (ls *.mp3 | sort -n) long.mp3`