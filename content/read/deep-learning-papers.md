---
title: Papers
tags: []
notebook: TIL
---

# Links to papers I've read and gleaned insights

## To Read:

### [NEURAL ARCHITECTURE SEARCH WITH REINFORCEMENT LEARNING](https://arxiv.org/pdf/1611.01578v1.pdf)
+

### [Learning What and Where to Draw](https://arxiv.org/abs/1610.02454)
+ Notes:

### [Recurrent Highway Networks](https://arxiv.org/abs/1607.03474)
+ Notes:

### [Bootstrapping Dialog Systems with Word Embeddings](http://www.cs.cmu.edu/~apparikh/nips2014ml-nlp/camera-ready/forgues_etal_mlnlp2014.pdf)
+ Notes:

### [Language Understanding for Text-based Games Using Deep Reinforcement Learning](https://arxiv.org/abs/1506.08941)
+ Notes:

### [Learning to Compose Neural Networks for Question Answering](http://arxiv.org/abs/1601.01705v4)
+ Notes:

### [Evaluating Prerequisite Qualities for Learning End-to-End Dialog Systems](http://arxiv.org/abs/1511.06931)
+ Notes:

### [Learning Phrase Representations using RNN Encoder-Decoder for Statistical Machine Translation](http://arxiv.org/abs/1406.1078)
+ Notes:

### [DeepMath - Deep Sequence Models for Premise Selection](https://arxiv.org/abs/1606.04442)
+ Notes:

----------------------
# NLP Oriented

### [Building End-To-End Dialogue Systems Using Generative Hierarchical Neural Network Models](http://arxiv.org/abs/1507.04808)
+ Date: Jul 3, 2016
+ Notes:
    + Initialized their word embeddings with word2vec trained on google news dataset
    + Used


### [End-to-end LSTM-based dialog control optimized with supervised and reinforcement learning](https://arxiv.org/pdf/1606.01269v1.pdf)
+ Date: Jul 2, 2016
+ Notes:
    + 3 part system
        + lstm - 32 cells, 1 layer deep
        + parser
        + state based software api thing

### [Matching Networks for One Shot Learning](http://arxiv.org/pdf/1606.04080v1.pdf)
+ Date: 6/15/16
+ Notes:
    +


### [Learning End-to-End Goal-Oriented Dialog](http://arxiv.org/abs/1605.07683)

+ Date: 6/5/16
+ Notes:
    + storing conversational data (sec 4.)


### [Globally Normalized Transition-Based Neural Networks](http://arxiv.org/abs/1603.06042)
+ Date: 6/10/16
+ notes:
    + A.k.a. syntaxnet
    + Feedforward network.  Basis of improvement on normalization versus LSTM/residual networks.
    + Original google blog post [here](https://research.googleblog.com/2016/05/announcing-syntaxnet-worlds-most.html).


| Title: | [Globally Normalized Transition-Based Neural Networks](http://arxiv.org/abs/1603.06042)
|--------|----------------------
| date:  | 6/10/16
+ notes:
    + a.k.a. syntaxnet.
    + Feedforward network.  Basis of improvement on normalization versus LSTM/residual networks.
    + Original google blog post [here](https://research.googleblog.com/2016/05/announcing-syntaxnet-worlds-most.html).


### [Sequence to Sequence Learning with Neural Networks](http://arxiv.org/abs/1409.3215)

Notes:


----------------------
# CV/CNN Oriented


----------------------
# RL Oriented

### [Deep Reinforcement Learning: Pong from Pixels](http://karpathy.github.io/2016/05/31/rl/)
+ Date: 6/15/16
+ notes:
    +

----------------------
# Templates:

+ Date:
+ Notes:

| Title: | title-of-paper
|--------|----------------------
| date:  | date-read
| notes: | general notes