# Larger Books or Topics Other Than White papers <sup> <a href="#fnbooks">[1]</a></sup>

## Read:

## To Read:

## Primer:
+ [How to read a book](http://pne.people.si.umich.edu/PDF/howtoread.pdf)

### [A Primer on Neural Network Models for Natural Language Processing](http://arxiv.org/abs/1510.00726)
+ date: Jun 16, 2016
+ Author: yoav goldberg
+ Notes:
    + Dense Embeddings vs One-Hot
        + One-hot doesn't account for word similarity, sparse input vectors perform poorly generally
        + Dense is computationally more efficient and one-hot can become impractical with large corpus

### [Deep Learning Book](http://www.deeplearningbook.org/)
+ notes:
    + authors: Ian Goodfellow Yoshua Bengio and Aaron Courville

# Templates:

+ Date:
+ Notes:

| Title: | title-of-paper
|--------|----------------
| date:  | date-read
| notes: | general notes

--------------

<p><a name="fnbooks">[1] more than 20 pages usually</a> </p>
