# to use command/script/etc in fish with env variable

i.e. for something like tensorflow in bash you would configure with
`TF_UNOFFICIAL_SETTING=1 ./configure`


you must explicitly state env i.e.:
`env TIME_COP=2016-02-24 rake app:send_daily_notifications`

or for tensorflow example:
`env TF_UNOFFICIAL_SETTING=1 ./configure`
