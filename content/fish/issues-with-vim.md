# occasional issues with vim

from: [this gist](https://gist.github.com/jiecuoren/0d0bff2efa48e03c2fc5)

add `set -x SHELL /bin/bash` to config.fish if

```
when use fish, something you will find error: E79: Cannot expand wildcards
Raw
```
