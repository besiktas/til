import requests


url = "https://gitlab.com/api/v4/runners"
headers = {"PRIVATE-TOKEN": "YOUR_PRIVATE_TOKEN_HERE"}

r = requests.get(url, headers=headers)

runners = r.json()

for runner in runners:
    if runner['description'] != 'SOME_DESCRIPTION':
        d = requests.delete("https://gitlab.com/api/v4/runners/" + str(runner['id']), headers=headers)