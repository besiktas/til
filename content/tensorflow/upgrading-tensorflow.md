# upgrading tensorflow

for instance to upgrade from r0.8 to r0.9 on mac with GPU:

1. pip3 uninstall tensorflow
2. cd tensorflow
3. TF_UNOFFICIAL_SETTING=1 ./configure
    - Default python3 location: /usr/local/bin/python3
    - Cuda SDK version: 7.5
    - Cuda compute capability: 3.0
4. bazel build -c opt --config=cuda //tensorflow/tools/pip_package:build_pip_package
5. bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp/tensorflow_pkg
6. pip3 install /tmp/tensorflow_pkg/tensorflow-0.9.0-py3-none-any.whl


While this is specifically for python3, works identical for python2 as well.