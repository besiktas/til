# using awk instead of grep

```bash
gcloud compute instances list | awk '/dashboard/ {print $5}'
```

take the line containing dashboard and get 5th column 