# using curl to post a binary

to post a binary (say a music file) for testing a server upload or something:

```bash
curl  -H "Content-Type: audio/flac" --data @tmp/transcribe.flac http://0.0.0.0:8000/audio
```

this could be received then in a sanic server (or flask) on the request.data object that is received 

i.e. with sanic
```python
@app.route('/audio', methods=['POST'])
async def upload_file(request):
    if request.method == 'POST':
        savedfile = save_audio(request.body)
    return "ok"
```