#!/bin/bash

make_header() {
    title_=$1
    date_="2019-10-04"
    HEADER_STRING="---
title: "${title_}"
date: "${date_}"
---
"

    echo "${HEADER_STRING}" > tmp.md
}


make_header "example titl2e"